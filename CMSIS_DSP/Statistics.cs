using System;
using System.Runtime.CompilerServices;
using Microsoft.SPOT;

namespace ARM.CMSIS.DSP
{
    /// <summary>
    /// Implements CMSIS statistic funtions
    /// </summary>
    public static  class Statistics
    {
        /// <summary>
        /// Gets the maximum of a vector
        /// </summary>
        /// <param name="source">Vector.</param>
        /// <returns>Position of the maximum value.</returns>
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern static uint Maximum(float[] source);
        /// <summary>
        /// Get the minimum of a vector
        /// </summary>
        /// <param name="source">Vector</param>
        /// <returns>Position of the minimum value.</returns>
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern static uint Minimum(float[] source);
        /// <summary>
        /// Gets the mean of a vector.
        /// </summary>
        /// <param name="source">Vector.</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern static float Mean(float[] source);
        /// <summary>
        /// Gets the sum of the square of each value of a vector.
        /// </summary>
        /// <param name="source">Vector.</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern static float Power(float[] source);
        /// <summary>
        /// Gets the RMS (Root mean square) value of a vector.
        /// </summary>
        /// <param name="source">Vector</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern static float RMS(float[] source);
        /// <summary>
        /// Gets the standard deviation of a vector.
        /// </summary>
        /// <param name="source">Vector.</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern static float StdDev(float[] source);
        /// <summary>
        /// Gets the variance of a vector.
        /// </summary>
        /// <param name="source">Vector.</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern static float Variance(float[] source);
    }
}
