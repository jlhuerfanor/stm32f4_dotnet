/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2015, Jerson Leonardo Huerfano Romero

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Runtime.CompilerServices;
using Microsoft.SPOT;

namespace ARM.CMSIS.DSP
{
    /// <summary>
    /// Reference to the ARM Implementation of Biquad Filter
    /// </summary>
    public static class BiquadFilterARM
    {
        /// <summary>
        /// Filter the input and returns the filtered data.
        /// </summary>
        /// <param name="nStages">Number of stages of the filter.</param>
        /// <param name="state">States of the fiter.</param>
        /// <param name="coefs">Coeficients of the filter.</param>
        /// <param name="input">Data input</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern float ProcessARM(uint nStages, float[] state, float[] coefs, float input);
        /// <summary>
        /// Filter a set of data values and stores the result on a vector.
        /// </summary>
        /// <param name="nStages">Number of stages of the filter.</param>
        /// <param name="state">States of the fiter.</param>
        /// <param name="coefs">Coefficients of the filter.</param>
        /// <param name="input">Input vector.</param>
        /// <param name="output">Output vector.</param>
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void ProcessARM(uint nStages, float[] state, float[] coefs, float[] input, float[] output);
    }
}
