using System;
using System.Runtime.CompilerServices;
using Microsoft.SPOT;

namespace ARM.CMSIS.DSP
{
    /// <summary>
    /// Implements the CMSIS FIR Filter.
    /// </summary>
    public static class FIRFilterARM
    {
        /// <summary>
        /// Filters a set of values and store the output in a vector.
        /// </summary>
        /// <param name="coeffs">Coefficients of the filter.</param>
        /// <param name="states">States of the filter.</param>
        /// <param name="data">Data to be filtered</param>
        /// <param name="output">Result of the filter.</param>
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern static void ProcessARM(float[] coeffs, float[] states, float[] data, float[] output);
        /// <summary>
        /// Filters a set of values and returns the filtered data.
        /// </summary>
        /// <param name="coeffs">Coefficients of the filter.</param>
        /// <param name="states">States of the  filter.</param>
        /// <param name="data">Data to be filtered.</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern static float ProcessARM(float[] coeffs, float[] states, float data);
    }
}
