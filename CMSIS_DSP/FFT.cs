/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2015, Jerson Leonardo Huerfano Romero

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Runtime.CompilerServices;
using Microsoft.SPOT;

namespace ARM.CMSIS.DSP
{
    /// <summary>
    /// Implements CMSIS FFT Functions.
    /// </summary>
    public static class FFT
    {
        /// <summary>
        /// Computes the Fast Fourier Transform of the input and returns the magnitude
        /// of the FFT components
        /// </summary>
        /// <param name="input">Input with size N = {32, 64, ... , 4096, 8192}, in complex form: x[n] = realPart, x[n+1] = complexPart</param>
        /// <param name="output">The magnitude of the FFT with size N/2</param>
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void FFTMagnitude(float[] input, float[] output);
        /// <summary>
        /// Calculates the Complex Fast Fourier Transform of the input. Processing occurs in-place.
        /// </summary>
        /// <param name="cfft_io">Input of the CFFT with size N = {32, 64, ... , 4096, 8192}, in complex form: x[n] = realPart, x[n+1] = complexPart
        /// The output will be placed in this array.</param>
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void FFTComplex(float[] cfft_io);
        /// <summary>
        /// Calculates the Inverse of the Complex Fast Fourier Transform of the input. Processing occurs in-place.
        /// </summary>
        /// <param name="cfft_io">Input of the ICFFT with size N = {32, 64, ... , 4096, 8192}, in complex form: x[n] = realPart, x[n+1] = complexPart
        /// The output will be placed in this array.</param>
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void ICFFTComplex(float[] cfft_io);
    }
}
