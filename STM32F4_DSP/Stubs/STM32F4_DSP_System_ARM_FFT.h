//-----------------------------------------------------------------------------
//
//                   ** WARNING! ** 
//    This file was generated automatically by a tool.
//    Re-running the tool will overwrite this file.
//    You should copy this file to a custom location
//    before adding any customization in the copy to
//    prevent loss of your changes when the tool is
//    re-run.
//
//-----------------------------------------------------------------------------


#ifndef _STM32F4_DSP_SYSTEM_ARM_FFT_H_
#define _STM32F4_DSP_SYSTEM_ARM_FFT_H_

namespace System
{
    namespace ARM
    {
        struct FFT
        {
            // Helper Functions to access fields of managed object
            // Declaration of stubs. These functions are implemented by Interop code developers
            static void FFTMagnitude( CLR_RT_TypedArray_float param0, CLR_RT_TypedArray_float param1, HRESULT &hr );
            static void FFTComplex( CLR_RT_TypedArray_float param0, HRESULT &hr );
            static void ICFFTComplex( CLR_RT_TypedArray_float param0, HRESULT &hr );
        };
    }
}
#endif  //_STM32F4_DSP_SYSTEM_ARM_FFT_H_
