/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2015, Jerson Leonardo Huerfano Romero

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Runtime.CompilerServices;
using Microsoft.SPOT;

namespace System.ARM
{
    /// <summary>
    /// Reference to the ARM Implementation of Biquad Filter
    /// </summary>
    internal static class BiquadFilterARM
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern float ProcessARM(uint nStages, float[] state, float[] coefs, float input);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void ProcessARM(uint nStages, float[] state, float[] coefs, float[] input, float[] output);
    }
}
