using System;
using System.MicroFramework;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using System.MicroFramework.DataAcquisition;

namespace MFTest
{
    public class TestApplication : MFApplication
    {
        float[] f;
        System.ARM.BiquadFilter filter;

        protected override void ApplicationSetup()
        {
            f = new float[512];

            for (int i = 0; i < f.Length; i++)
            {
                f[i] = (float)System.Math.Sin(2.0 * System.Math.PI * i * 60 / f.Length) +
                         (float)System.Math.Sin(2.0 * System.Math.PI * i / f.Length);
            }

            filter = new System.ARM.BiquadFilter(1);

            float cos_t = (float)System.Math.Cos(2 * 3.141592f * 60 / f.Length);

            filter.SetAllCoefficients(1f, -2f * cos_t, 1f, 2 * 0.5f * cos_t, -0.25f);
            
        }

        protected override void ApplicationStartup()
        {
            for (int i = 0; i < f.Length; i++)
            {
                float o = filter.Process(f[i]);
                Debug.Print(o.ToString());
            }
        }
    }
}
