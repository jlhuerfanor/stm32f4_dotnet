using System;
using System.MicroFramework;
using Microsoft.SPOT;


namespace MFTest
{
    public class Program
    {
        public static void Main()
        {
            Debug.Print(System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());

            MFApplication.Current = new TestApplication();
            MFApplication.Current.Run();
        }
    }
}
