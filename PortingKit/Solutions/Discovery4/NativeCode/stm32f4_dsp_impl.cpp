#include "stm32f4_dsp_impl.h"

void ARM_CFFT(CLR_RT_TypedArray_float vector, uint8_t ifftFlag)
{
	UINT32 length = vector.GetSize() / 2;

	switch (length)
	{
	case 16:
		arm_cfft_f32(&arm_cfft_sR_f32_len16, vector.GetBuffer(), ifftFlag, 1);
		break;
	case 32:
		arm_cfft_f32(&arm_cfft_sR_f32_len32, vector.GetBuffer(), ifftFlag, 1);
		break;
	case 64:
		arm_cfft_f32(&arm_cfft_sR_f32_len64, vector.GetBuffer(), ifftFlag, 1);
		break;
	case 128:
		arm_cfft_f32(&arm_cfft_sR_f32_len128, vector.GetBuffer(), ifftFlag, 1);
		break;
	case 256:
		arm_cfft_f32(&arm_cfft_sR_f32_len256, vector.GetBuffer(), ifftFlag, 1);
		break;
	case 512:
		arm_cfft_f32(&arm_cfft_sR_f32_len512, vector.GetBuffer(), ifftFlag, 1);
		break;
	case 1024:
		arm_cfft_f32(&arm_cfft_sR_f32_len1024, vector.GetBuffer(), ifftFlag, 1);
		break;
	case 2048:
		arm_cfft_f32(&arm_cfft_sR_f32_len2048, vector.GetBuffer(), ifftFlag, 1);
		break;
	case 4096:
		arm_cfft_f32(&arm_cfft_sR_f32_len4096, vector.GetBuffer(), ifftFlag, 1);
		break;
	default: break;
	}
}
void ARM_MAG_FFT(CLR_RT_TypedArray_float vector, CLR_RT_TypedArray_float output)
{
	ARM_CFFT(vector, 0);
	arm_cmplx_mag_f32(vector.GetBuffer(), output.GetBuffer(), vector.GetSize() / 2);
}
float ARM_IIR_BIQUAD(UINT32 param0, CLR_RT_TypedArray_float param1, CLR_RT_TypedArray_float param2, float param3)
{
	float input = param3, output = 0;
	arm_biquad_casd_df1_inst_f32 iir_filter;

	iir_filter.numStages = param0;
	iir_filter.pState = param1.GetBuffer();
	iir_filter.pCoeffs = param2.GetBuffer();

	arm_biquad_cascade_df1_f32(&iir_filter, &input, &output, 1);

	return output;
}