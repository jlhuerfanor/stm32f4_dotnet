//-----------------------------------------------------------------------------
//
//                   ** WARNING! ** 
//    This file was generated automatically by a tool.
//    Re-running the tool will overwrite this file.
//    You should copy this file to a custom location
//    before adding any customization in the copy to
//    prevent loss of your changes when the tool is
//    re-run.
//
//-----------------------------------------------------------------------------


#include "STM32F4_DSP.h"
#include "STM32F4_DSP_System_ARM_FFT.h"
#include <arm_math.h>
#include <arm_const_structs.h>

using namespace System::ARM;

void ARM_CFFT(CLR_RT_TypedArray_float vector, uint8_t ifftFlag)
{
	UINT32 length = vector.GetSize() / 2;

	switch (length)
	{
	case 16:
		arm_cfft_f32(&arm_cfft_sR_f32_len16, vector.GetBuffer(), ifftFlag, 1);
		break;
	case 32:
		arm_cfft_f32(&arm_cfft_sR_f32_len32, vector.GetBuffer(), ifftFlag, 1);
		break;
	case 64:
		arm_cfft_f32(&arm_cfft_sR_f32_len64, vector.GetBuffer(), ifftFlag, 1);
		break;
	case 128:
		arm_cfft_f32(&arm_cfft_sR_f32_len128, vector.GetBuffer(), ifftFlag, 1);
		break;
	case 256:
		arm_cfft_f32(&arm_cfft_sR_f32_len256, vector.GetBuffer(), ifftFlag, 1);
		break;
	case 512:
		arm_cfft_f32(&arm_cfft_sR_f32_len512, vector.GetBuffer(), ifftFlag, 1);
		break;
	case 1024:
		arm_cfft_f32(&arm_cfft_sR_f32_len1024, vector.GetBuffer(), ifftFlag, 1);
		break;
	case 2048:
		arm_cfft_f32(&arm_cfft_sR_f32_len2048, vector.GetBuffer(), ifftFlag, 1);
		break;
	case 4096:
		arm_cfft_f32(&arm_cfft_sR_f32_len4096, vector.GetBuffer(), ifftFlag, 1);
		break;
	default: break;
	}
}
void ARM_MAG_FFT(CLR_RT_TypedArray_float vector, CLR_RT_TypedArray_float output)
{
	ARM_CFFT(vector, 0);
	arm_cmplx_mag_f32(vector.GetBuffer(), output.GetBuffer(), vector.GetSize() / 2);
}

void FFT::FFTMagnitude( CLR_RT_TypedArray_float param0, CLR_RT_TypedArray_float param1, HRESULT &hr )
{
	ARM_MAG_FFT(param0, param1);
}

void FFT::FFTComplex( CLR_RT_TypedArray_float param0, HRESULT &hr )
{
	ARM_CFFT(param0, 0);
}

void FFT::ICFFTComplex( CLR_RT_TypedArray_float param0, HRESULT &hr )
{
	ARM_CFFT(param0, 1);
}

