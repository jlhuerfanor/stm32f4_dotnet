//-----------------------------------------------------------------------------
//
//                   ** WARNING! ** 
//    This file was generated automatically by a tool.
//    Re-running the tool will overwrite this file.
//    You should copy this file to a custom location
//    before adding any customization in the copy to
//    prevent loss of your changes when the tool is
//    re-run.
//
//-----------------------------------------------------------------------------


#include "STM32F4_DSP.h"
#include "STM32F4_DSP_System_ARM_BiquadFilterARM.h"
#include <arm_math.h>

using namespace System::ARM;

inline float ARM_IIR_BIQUAD(UINT32 param0, CLR_RT_TypedArray_float param1, CLR_RT_TypedArray_float param2, float param3)
{
	float input = param3, output = 0;
	arm_biquad_casd_df1_inst_f32 iir_filter;

	iir_filter.numStages = param0;
	iir_filter.pState = param1.GetBuffer();
	iir_filter.pCoeffs = param2.GetBuffer();

	arm_biquad_cascade_df1_f32(&iir_filter, &input, &output, 1);

	return output;
}

float BiquadFilterARM::ProcessARM( UINT32 param0, CLR_RT_TypedArray_float param1, CLR_RT_TypedArray_float param2, float param3, HRESULT &hr )
{
	return ARM_IIR_BIQUAD(param0, param1, param2, param3);
}

void BiquadFilterARM::ProcessARM( UINT32 param0, CLR_RT_TypedArray_float param1, CLR_RT_TypedArray_float param2, CLR_RT_TypedArray_float param3, CLR_RT_TypedArray_float param4, HRESULT &hr )
{
}

