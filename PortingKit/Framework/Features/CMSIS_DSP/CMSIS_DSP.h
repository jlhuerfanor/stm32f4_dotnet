//-----------------------------------------------------------------------------
//
//    ** DO NOT EDIT THIS FILE! **
//    This file was generated by a tool
//    re-running the tool will overwrite this file.
//
//-----------------------------------------------------------------------------


#ifndef _CMSIS_DSP_H_
#define _CMSIS_DSP_H_

#include <TinyCLR_Interop.h>
struct Library_CMSIS_DSP_ARM_CMSIS_DSP_BiquadFilterARM
{
    TINYCLR_NATIVE_DECLARE(ProcessARM___STATIC__R4__U4__SZARRAY_R4__SZARRAY_R4__R4);
    TINYCLR_NATIVE_DECLARE(ProcessARM___STATIC__VOID__U4__SZARRAY_R4__SZARRAY_R4__SZARRAY_R4__SZARRAY_R4);

    //--//

};

struct Library_CMSIS_DSP_ARM_CMSIS_DSP_FFT
{
    TINYCLR_NATIVE_DECLARE(FFTMagnitude___STATIC__VOID__SZARRAY_R4__SZARRAY_R4);
    TINYCLR_NATIVE_DECLARE(FFTComplex___STATIC__VOID__SZARRAY_R4);
    TINYCLR_NATIVE_DECLARE(ICFFTComplex___STATIC__VOID__SZARRAY_R4);

    //--//

};

struct Library_CMSIS_DSP_ARM_CMSIS_DSP_FIRFilterARM
{
    TINYCLR_NATIVE_DECLARE(ProcessARM___STATIC__VOID__SZARRAY_R4__SZARRAY_R4__SZARRAY_R4__SZARRAY_R4);
    TINYCLR_NATIVE_DECLARE(ProcessARM___STATIC__R4__SZARRAY_R4__SZARRAY_R4__R4);

    //--//

};

struct Library_CMSIS_DSP_ARM_CMSIS_DSP_Statistics
{
    TINYCLR_NATIVE_DECLARE(Maximum___STATIC__U4__SZARRAY_R4);
    TINYCLR_NATIVE_DECLARE(Minimum___STATIC__U4__SZARRAY_R4);
    TINYCLR_NATIVE_DECLARE(Mean___STATIC__R4__SZARRAY_R4);
    TINYCLR_NATIVE_DECLARE(Power___STATIC__R4__SZARRAY_R4);
    TINYCLR_NATIVE_DECLARE(RMS___STATIC__R4__SZARRAY_R4);
    TINYCLR_NATIVE_DECLARE(StdDev___STATIC__R4__SZARRAY_R4);
    TINYCLR_NATIVE_DECLARE(Variance___STATIC__R4__SZARRAY_R4);

    //--//

};



extern const CLR_RT_NativeAssemblyData g_CLR_AssemblyNative_CMSIS_DSP;

#endif  //_CMSIS_DSP_H_
