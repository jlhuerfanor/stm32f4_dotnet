//-----------------------------------------------------------------------------
//
//                   ** WARNING! ** 
//    This file was generated automatically by a tool.
//    Re-running the tool will overwrite this file.
//    You should copy this file to a custom location
//    before adding any customization in the copy to
//    prevent loss of your changes when the tool is
//    re-run.
//
//-----------------------------------------------------------------------------


#include "CMSIS_DSP.h"
#include "CMSIS_DSP_ARM_CMSIS_DSP_BiquadFilterARM.h"
#include <arm_math.h>

using namespace ARM::CMSIS::DSP;

void ARM_IIR_BIQUAD(UINT32 param0, CLR_RT_TypedArray_float param1, CLR_RT_TypedArray_float param2, float * param3, float * param4, UINT32 param5)
{
	arm_biquad_casd_df1_inst_f32 iir_filter;

	iir_filter.numStages = param0;
	iir_filter.pState = param1.GetBuffer();
	iir_filter.pCoeffs = param2.GetBuffer();

	arm_biquad_cascade_df1_f32(&iir_filter, param3, param4, param5);
}

float BiquadFilterARM::ProcessARM( UINT32 param0, CLR_RT_TypedArray_float param1, CLR_RT_TypedArray_float param2, float param3, HRESULT &hr )
{
	float retVal = 0;
	float input = param3;

	ARM_IIR_BIQUAD(param0, param1, param2, &input, &retVal, 1);

	return retVal;
}

void BiquadFilterARM::ProcessARM( UINT32 param0, CLR_RT_TypedArray_float param1, CLR_RT_TypedArray_float param2, CLR_RT_TypedArray_float param3, CLR_RT_TypedArray_float param4, HRESULT &hr )
{
	ARM_IIR_BIQUAD(param0, param1, param2, param3.GetBuffer(), param4.GetBuffer(), param3.GetSize());
}

