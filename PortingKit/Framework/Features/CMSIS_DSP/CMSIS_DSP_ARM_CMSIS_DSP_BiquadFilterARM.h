//-----------------------------------------------------------------------------
//
//                   ** WARNING! ** 
//    This file was generated automatically by a tool.
//    Re-running the tool will overwrite this file.
//    You should copy this file to a custom location
//    before adding any customization in the copy to
//    prevent loss of your changes when the tool is
//    re-run.
//
//-----------------------------------------------------------------------------


#ifndef _CMSIS_DSP_ARM_CMSIS_DSP_BIQUADFILTERARM_H_
#define _CMSIS_DSP_ARM_CMSIS_DSP_BIQUADFILTERARM_H_

namespace ARM
{
    namespace CMSIS
    {
        namespace DSP
        {
            struct BiquadFilterARM
            {
                // Helper Functions to access fields of managed object
                // Declaration of stubs. These functions are implemented by Interop code developers
                static float ProcessARM( UINT32 param0, CLR_RT_TypedArray_float param1, CLR_RT_TypedArray_float param2, float param3, HRESULT &hr );
                static void ProcessARM( UINT32 param0, CLR_RT_TypedArray_float param1, CLR_RT_TypedArray_float param2, CLR_RT_TypedArray_float param3, CLR_RT_TypedArray_float param4, HRESULT &hr );
            };
        }
    }
}
#endif  //_CMSIS_DSP_ARM_CMSIS_DSP_BIQUADFILTERARM_H_
