//-----------------------------------------------------------------------------
//
//                   ** WARNING! ** 
//    This file was generated automatically by a tool.
//    Re-running the tool will overwrite this file.
//    You should copy this file to a custom location
//    before adding any customization in the copy to
//    prevent loss of your changes when the tool is
//    re-run.
//
//-----------------------------------------------------------------------------


#include "CMSIS_DSP.h"
#include "CMSIS_DSP_ARM_CMSIS_DSP_Statistics.h"
#include <arm_math.h>

using namespace ARM::CMSIS::DSP;

UINT32 Statistics::Maximum( CLR_RT_TypedArray_float param0, HRESULT &hr )
{
	float val = 0;
	UINT32 retVal = 0;

	arm_max_f32(param0.GetBuffer(), param0.GetSize(), &val, &retVal);
     
    return retVal;
}

UINT32 Statistics::Minimum( CLR_RT_TypedArray_float param0, HRESULT &hr )
{
    UINT32 retVal = 0; 
	float val = 0;

	arm_min_f32(param0.GetBuffer(), param0.GetSize(), &val, &retVal);

    return retVal;
}

float Statistics::Mean( CLR_RT_TypedArray_float param0, HRESULT &hr )
{
    float retVal = 0; 

	arm_mean_f32(param0.GetBuffer(), param0.GetSize(), &retVal);

    return retVal;
}

float Statistics::Power( CLR_RT_TypedArray_float param0, HRESULT &hr )
{
    float retVal = 0; 

	arm_power_f32(param0.GetBuffer(), param0.GetSize(), &retVal);

    return retVal;
}

float Statistics::RMS( CLR_RT_TypedArray_float param0, HRESULT &hr )
{
    float retVal = 0; 

	arm_rms_f32(param0.GetBuffer(), param0.GetSize(), &retVal);

    return retVal;
}

float Statistics::StdDev( CLR_RT_TypedArray_float param0, HRESULT &hr )
{
    float retVal = 0; 

	arm_std_f32(param0.GetBuffer(), param0.GetSize(), &retVal);

    return retVal;
}

float Statistics::Variance( CLR_RT_TypedArray_float param0, HRESULT &hr )
{
    float retVal = 0; 

	arm_var_f32(param0.GetBuffer(), param0.GetSize(), &retVal);

    return retVal;
}

