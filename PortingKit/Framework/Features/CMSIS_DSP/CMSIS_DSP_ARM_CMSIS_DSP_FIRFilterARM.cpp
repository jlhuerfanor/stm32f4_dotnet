//-----------------------------------------------------------------------------
//
//                   ** WARNING! ** 
//    This file was generated automatically by a tool.
//    Re-running the tool will overwrite this file.
//    You should copy this file to a custom location
//    before adding any customization in the copy to
//    prevent loss of your changes when the tool is
//    re-run.
//
//-----------------------------------------------------------------------------


#include "CMSIS_DSP.h"
#include "CMSIS_DSP_ARM_CMSIS_DSP_FIRFilterARM.h"
#include <arm_math.h>

using namespace ARM::CMSIS::DSP;

void FIRFilterARM::ProcessARM( CLR_RT_TypedArray_float param0, CLR_RT_TypedArray_float param1, CLR_RT_TypedArray_float param2, CLR_RT_TypedArray_float param3, HRESULT &hr )
{
	arm_fir_instance_f32 S;
	uint32_t i;

	uint32_t blockSize = param1.GetSize() - param0.GetSize() + 1;
	uint32_t numBlocks = param2.GetSize() / blockSize;
	uint32_t remain = param2.GetSize() % blockSize;

	S.numTaps = param0.GetSize();
	S.pCoeffs = param0.GetBuffer();
	S.pState = param1.GetBuffer();
	
	for (i = 0; i < numBlocks; i++)
		arm_fir_f32(&S, param2.GetBuffer() + (i * blockSize), param3.GetBuffer() + (i * blockSize), blockSize);

	if (remain > 0)
		arm_fir_f32(&S, param2.GetBuffer() + (numBlocks * blockSize), param3.GetBuffer() + (numBlocks * blockSize), remain);
}

float FIRFilterARM::ProcessARM( CLR_RT_TypedArray_float param0, CLR_RT_TypedArray_float param1, float param2, HRESULT &hr )
{
    float retVal = 0;
	float input = param2;

	arm_fir_instance_f32 S;
	
	S.numTaps = param0.GetSize();
	S.pCoeffs = param0.GetBuffer();
	S.pState = param1.GetBuffer();

	arm_fir_f32(&S, &input, &retVal, 1);

    return retVal;
}

