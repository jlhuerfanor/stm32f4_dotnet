//-----------------------------------------------------------------------------
//
//                   ** WARNING! ** 
//    This file was generated automatically by a tool.
//    Re-running the tool will overwrite this file.
//    You should copy this file to a custom location
//    before adding any customization in the copy to
//    prevent loss of your changes when the tool is
//    re-run.
//
//-----------------------------------------------------------------------------


#ifndef _CMSIS_DSP_ARM_CMSIS_DSP_STATISTICS_H_
#define _CMSIS_DSP_ARM_CMSIS_DSP_STATISTICS_H_

namespace ARM
{
    namespace CMSIS
    {
        namespace DSP
        {
            struct Statistics
            {
                // Helper Functions to access fields of managed object
                // Declaration of stubs. These functions are implemented by Interop code developers
                static UINT32 Maximum( CLR_RT_TypedArray_float param0, HRESULT &hr );
                static UINT32 Minimum( CLR_RT_TypedArray_float param0, HRESULT &hr );
                static float Mean( CLR_RT_TypedArray_float param0, HRESULT &hr );
                static float Power( CLR_RT_TypedArray_float param0, HRESULT &hr );
                static float RMS( CLR_RT_TypedArray_float param0, HRESULT &hr );
                static float StdDev( CLR_RT_TypedArray_float param0, HRESULT &hr );
                static float Variance( CLR_RT_TypedArray_float param0, HRESULT &hr );
            };
        }
    }
}
#endif  //_CMSIS_DSP_ARM_CMSIS_DSP_STATISTICS_H_
