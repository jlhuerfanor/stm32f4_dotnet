/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2015, Jerson Leonardo Huerfano Romero

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Microsoft.SPOT;

namespace System.MicroFramework.Control
{
    /// <summary>
    /// Defines the feedback vector output.
    /// </summary>
    public class FeedbackVector
    {
        /// <summary>
        /// Creates a new instance of FeedbackVector.
        /// </summary>
        public FeedbackVector()
        {
            Error = new float[2];
        }

        /// <summary>
        /// Gets or sets the error of the feedback: Error[1] is the current calculated error; Error[0] is the last calculated error.
        /// </summary>
        public float[] Error { get; private set; }

        /// <summary>
        /// Updates the vector values of the feedback.
        /// </summary>
        /// <param name="reference">Reference of the controller.</param>
        /// <param name="output">Output of the controlled system.</param>
        public void Update(float reference, float output)
        {
            Error[0] = Error[1];
            Error[1] = reference - output;
        }
        /// <summary>
        /// Returns the diference of the Error vector values.
        /// </summary>
        /// <returns></returns>
        public float Difference()
        {
            return Error[1] - Error[0];
        }
        /// <summary>
        /// Returns the sum of the Error vector values.
        /// </summary>
        /// <returns></returns>
        public float Add()
        {
            return Error[0] + Error[1];
        }
    }
}
