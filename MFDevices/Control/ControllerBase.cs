/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2015, Jerson Leonardo Huerfano Romero

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Microsoft.SPOT;

namespace System.MicroFramework.Control
{
    /// <summary>
    /// Defines the base class of a controller implementation.
    /// </summary>
    public abstract class ControllerBase : IDevice
    {
        /// <summary>
        /// Controller state change event handler.
        /// </summary>
        public event ControllerStateChangedEventHandler ControllerStateChanged;

        /// <summary>
        /// Creates a new instance of a ControllerBase
        /// </summary>
        /// <param name="daq">Instance of DataAcquisition.DAQ class.</param>
        /// <param name="dataIndex">Index of the data acquired from DataAcquisition.DAQ instance.</param>
        /// <param name="initialReference">Initial reference of the controller.</param>
        public ControllerBase(DataAcquisition.DAQ daq, int dataIndex, float initialReference)
        {
            this.Daq = daq;
            this.DataIndex = dataIndex;
            this.Reference = initialReference;
            this.ControllerState = new ControllerStateChangedEventArgs();
            this.Feedback = new FeedbackVector();

            daq.AcquisitionFinished += daq_AcquisitionFinished;
        }

        /// <summary>
        /// Creates a new instance of a ControllerBase
        /// </summary>
        /// <param name="daq">Instance of DataAcquisition.DAQ class.</param>
        /// <param name="dataIndex">Index of the data acquired from DataAcquisition.DAQ instance.</param>
        public ControllerBase(DataAcquisition.DAQ daq, int dataIndex)
            :this(daq, dataIndex, 0f) { }

        /// <summary>
        /// Gets the Data Acquisition System of the controller.
        /// </summary>
        public DataAcquisition.DAQ Daq { get; private set; }
        /// <summary>
        /// Gets the index of the data acquired from DataAcquisition.DAQ instance.
        /// </summary>
        public int DataIndex { get; private set; }
        /// <summary>
        /// Gets or sets the reference signal of the controller.
        /// </summary>
        public float Reference { get; set; }
        private ControllerStateChangedEventArgs ControllerState { get; set; }
        private FeedbackVector Feedback { get; set; }

        /// <summary>
        /// Calculates the control signal of the controller.
        /// </summary>
        /// <param name="feedback">Vector containing the feedback data.</param>
        /// <param name="elapsedTime">Sample time of the controller.</param>
        /// <returns></returns>
        protected abstract float GetControlSignal(FeedbackVector feedback, TimeSpan elapsedTime);
        private void daq_AcquisitionFinished(DataAcquisition.DAQ sender, DataAcquisition.AcquisitionFinishedEventArgs e)
        {
            Feedback.Update(Reference, e.Data[DataIndex]);

            ControllerState.ControlSignal = GetControlSignal(Feedback, e.ElapsedTime);

            if (ControllerStateChanged != null)
                ControllerStateChanged(this, ControllerState);
        }

        #region Miembros de IDevice

        int IDevice.ID { get; set; }

        void IDevice.Initialize() { }

        #endregion
        #region Miembros de IDisposable

        /// <summary>
        /// Dispose the controller resources.
        /// </summary>
        public void Dispose()
        {
            Daq.AcquisitionFinished -= daq_AcquisitionFinished;
            ControllerState = null;
            Feedback = null;

            Debug.GC(true);
        }

        #endregion
    }
}
