/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2015, Jerson Leonardo Huerfano Romero

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Microsoft.SPOT;

namespace System.MicroFramework.Control
{
    /// <summary>
    /// Implements a light controller listener through serial port.
    /// </summary>
    public class ControllerListener
    {
        /// <summary>
        /// Creates a new instance of ControllerListener
        /// </summary>
        /// <param name="port">Serial port instance</param>
        public ControllerListener(System.IO.Ports.SerialPort port)
        {
            this.SerialPort = port;
        }
        /// <summary>
        /// Creates a new instance of ControllerListener
        /// </summary>
        /// <param name="portName">Port name.</param>
        /// <param name="baudRate">Baud rate.</param>
        public ControllerListener(string portName, int baudRate)
        {
            this.SerialPort = new IO.Ports.SerialPort(portName, baudRate);
        }

        /// <summary>
        /// Gets the serial port instance.
        /// </summary>
        public System.IO.Ports.SerialPort SerialPort { get; private set; }

        /// <summary>
        /// Exposes a signed method to be atatched to a ControllerBase.ControllerStateChanged events.
        /// </summary>
        /// <param name="controller">Controler instance.</param>
        /// <param name="e">Controler state event args.</param>
        public void ControllerStateChanged(IDevice controller, ControllerStateChangedEventArgs e)
        {
            SerialPort.WriteByte(ControlHeader0);
            SerialPort.WriteByte(ControlHeader1);
            SerialPort.Write(BitConverter.GetBytes(controller.ID), 0, sizeof(int));
            SerialPort.Write(BitConverter.GetBytes(e.ControlSignal), 0, sizeof(float));
            SerialPort.Write(Serial.CommandPort.EOL, 0, Serial.CommandPort.EOL.Length);
        }

        /// <summary>
        /// Control report header byte 0
        /// </summary>
        public const byte ControlHeader0 = 0x05;
        /// <summary>
        /// Control report header byte 1
        /// </summary>
        public const byte ControlHeader1 = 0x0C;
    }
}
