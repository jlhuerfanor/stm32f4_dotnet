/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2015, Jerson Leonardo Huerfano Romero

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Microsoft.SPOT;

namespace System.MicroFramework.Control
{
    /// <summary>
    /// Implements a Proportional Controller.
    /// </summary>
    public class PController : ControllerBase
    {
        /// <summary>
        /// Creates a new instance of PController
        /// </summary>
        /// <param name="daq">Instance of DataAcquisition.DAQ class.</param>
        /// <param name="dataIndex">Index of the data acquired from DataAcquisition.DAQ instance.</param>
        /// <param name="kp">Proportional gain of the controller.</param>
        /// <param name="initialReference">Initial reference of the controller.</param>
        public PController(DataAcquisition.DAQ daq, int dataIndex, float kp, float initialReference)
            : base(daq, dataIndex, initialReference)
        {
            this.Kp = kp;
        }

        /// <summary>
        /// Creates a new instance of PController
        /// </summary>
        /// <param name="daq">Instance of DataAcquisition.DAQ class.</param>
        /// <param name="dataIndex">Index of the data acquired from DataAcquisition.DAQ instance.</param>
        /// <param name="kp">Proportional gain of the controller.</param>
        public PController(DataAcquisition.DAQ daq, int dataIndex, float kp)
            : this(daq, dataIndex, kp, 0) { }

        /// <summary>
        /// Gets or sets the proportional gain of the controller.
        /// </summary>
        public float Kp { get; set; }

        /// <summary>
        /// Calculates the control signal of the controller.
        /// </summary>
        /// <param name="feedback">Vector containing the feedback data.</param>
        /// <param name="elapsedTime">Sample time of the controller.</param>
        /// <returns></returns>
        protected override float GetControlSignal(FeedbackVector feedback, TimeSpan elapsedTime)
        {
            return Kp * feedback.Error[1];
        }
    }
}
