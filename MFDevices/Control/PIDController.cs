/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2015, Jerson Leonardo Huerfano Romero

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Microsoft.SPOT;

namespace System.MicroFramework.Control
{
    /// <summary>
    /// Implements a PID controller.
    /// </summary>
    public class PIDController : ControllerBase
    {
        /// <summary>
        /// Creates a new instance of PIDController
        /// </summary>
        /// <param name="daq">Instance of DataAcquisition.DAQ class.</param>
        /// <param name="dataIndex">Index of the data acquired from DataAcquisition.DAQ instance.</param>
        /// <param name="kp">The proportional gain of the error.</param>
        /// <param name="ki">The gain of the integral of the error.</param>
        /// <param name="kd">The gain of the derivative of the error.</param>
        /// <param name="initialReference">Initial reference of the controller.</param>
        public PIDController(DataAcquisition.DAQ daq, int dataIndex, float kp, float ki, float kd, float initialReference)
            : base(daq, dataIndex, initialReference)
        {
            this.Kp = kp;
            this.Ki = ki;
            this.Kd = kd;
        }

        /// <summary>
        /// Gets or sets the proportional gain of the error.
        /// </summary>
        public float Kp { get; set; }
        /// <summary>
        /// Gets or sets the gain of the integral of the error.
        /// </summary>
        public float Ki { get; set; }
        /// <summary>
        /// Gets or sets the gain of the derivative of the error.
        /// </summary>
        public float Kd { get; set; }

        private float iE { get; set; }

        /// <summary>
        /// Calculates the control signal of the controller.
        /// </summary>
        /// <param name="feedback">Vector containing the feedback data.</param>
        /// <param name="elapsedTime">Sample time of the controller.</param>
        /// <returns></returns>
        protected override float GetControlSignal(FeedbackVector feedback, TimeSpan elapsedTime)
        {
            float E = feedback.Error[1];
            float dE = feedback.Difference() / elapsedTime.ToSeconds();
            iE += feedback.Add() * elapsedTime.ToSeconds() / 2f;

            return (Kp * E) + (Ki * iE) + (Kd * dE);
        }
    }
}
