/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2015, Jerson Leonardo Huerfano Romero

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

namespace System.MicroFramework.DataAcquisition
{
    /// <summary>
    /// Implements an analog sensor.
    /// </summary>
    public class AnalogSensor : SensorBase
    {
        /// <summary>
        /// Creates a new instance of AnalogSensor.
        /// </summary>
        /// <param name="analogChannel">Analog channel</param>
        /// <param name="coefficients">Coefficients of the transformation.</param>
        public AnalogSensor(Cpu.AnalogChannel analogChannel, params float[] coefficients)
        {
            this.AnalogChannnel = analogChannel;
            this.Coefficients = coefficients;
        }

        /// <summary>
        /// Gets the AnalogChannnel of the sensor.
        /// </summary>
        public Cpu.AnalogChannel AnalogChannnel { get; private set; }
        private AnalogInput AnalogPort { get; set; }
        /// <summary>
        /// Gets the coefficients of the transformation.
        /// </summary>
        public float[] Coefficients { get; private set; }

        /// <summary>
        /// Reads the value of the sensor and apply the transformation.
        /// </summary>
        /// <returns></returns>
        protected override float Read_Pure()
        {
            float x = AnalogPort.ReadRaw();
            float y = 0f;
            float pow = 1;

            for (int i = 0; i < Coefficients.Length; i++)
            {
                y += Coefficients[i] * pow;
                pow *= x;
            }

            return y;
        }

        /// <summary>
        /// Initialize the sensor.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();

            AnalogPort = new AnalogInput(AnalogChannnel);
        }
    }
}
