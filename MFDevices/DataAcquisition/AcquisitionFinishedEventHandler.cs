/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2015, Jerson Leonardo Huerfano Romero

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Microsoft.SPOT;

namespace System.MicroFramework.DataAcquisition
{
    /// <summary>
    /// Handler model of Acquisition Finished Event.
    /// </summary>
    /// <param name="sender">Data Acquisition System witch raised the event.</param>
    /// <param name="e">Arguments of the acquisition.</param>
    public delegate void AcquisitionFinishedEventHandler(DAQ sender, AcquisitionFinishedEventArgs e);
}
