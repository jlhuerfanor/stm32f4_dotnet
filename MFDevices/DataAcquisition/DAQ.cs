/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2015, Jerson Leonardo Huerfano Romero

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using Microsoft.SPOT;

namespace System.MicroFramework.DataAcquisition
{
    /// <summary>
    /// Implements a Data Acquisition System, witch handles a set of sensors.
    /// </summary>
    public class DAQ : IDevice
    {
        /// <summary>
        /// Creates a new instance of DAQ.
        /// </summary>
        /// <param name="samplingTime">Sampling time, in ms</param>
        public DAQ(int samplingTime)
        {
            this.SamplingTime = samplingTime;
        }

        /// <summary>
        /// Acquisition finished event handler
        /// </summary>
        public event AcquisitionFinishedEventHandler AcquisitionFinished;
        /// <summary>
        /// Gets the list of sensors of the DAQ.
        /// </summary>
        public ArrayList Sensors { get; private set; }
        /// <summary>
        /// Gets the sampling time, in ms.
        /// </summary>
        public int SamplingTime { get; private set; }
        
        private ExtendedTimer Timer { get; set; }
        private AcquisitionFinishedEventArgs AcquisitionData { get; set; }
        private TimeSpan Elapsed { get; set; }

        private void OnTimerCallback(object userState)
        {
            if (!MFApplication.Current.IsActiveContext) return;

            for (int i = 0; i < Sensors.Count; i++)
            {
                var sensor = Sensors[i] as SensorBase;
                AcquisitionData.Data[i] = sensor.Read();
            }

            AcquisitionData.ElapsedTime = Timer.LastExpiration - Elapsed;
            Elapsed = Timer.LastExpiration;

            if (AcquisitionFinished != null)
                AcquisitionFinished(this, AcquisitionData);
        }

        /// <summary>
        /// Start a sampling routine.
        /// </summary>
        public void Start()
        {
            Timer.Change(0, SamplingTime);
        }
        /// <summary>
        /// Stop the current sampling routine.
        /// </summary>
        public void Stop()
        {
            Timer.Change(0, System.Threading.Timeout.Infinite);
        }

        #region Miembros de IDevice

        int IDevice.ID { get; set; }

        void IDevice.Initialize()
        {
            AcquisitionData = new AcquisitionFinishedEventArgs(Sensors.Count);
            Timer = new ExtendedTimer(OnTimerCallback, null, 0, System.Threading.Timeout.Infinite);
        }

        #endregion
        #region Miembros de IDisposable

        /// <summary>
        /// Dispose the sensor resources.
        /// </summary>
        public void Dispose()
        {
            Timer.Dispose();
        }

        #endregion
    }
}
