/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2015, Jerson Leonardo Huerfano Romero

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Microsoft.SPOT;

namespace System.MicroFramework.DataAcquisition
{
    /// <summary>
    /// Defines the base class of a sensor.
    /// </summary>
    public abstract class SensorBase : IDevice
    {

        /// <summary>
        /// Reads the value of the sensor.
        /// </summary>
        /// <returns></returns>
        protected abstract float Read_Pure();

        /// <summary>
        /// Reads the value of the sensor and applies the filter, if exists.
        /// </summary>
        /// <returns></returns>
        public float Read()
        {
            if (Filter != null)
                return Filter.Process(Read_Pure());
            else return Read_Pure();
        }
        /// <summary>
        /// Gets or sets the Filter instance.
        /// </summary>
        public SignalProcessing.IFilter Filter { get; set; }

        #region Miembros de IDevice


        int IDevice.ID { get; set; }

        /// <summary>
        /// Initialize the sensor.
        /// </summary>
        public virtual void Initialize() { }

        #endregion
        #region Miembros de IDisposable

        /// <summary>
        /// Dispose the sensor resources.
        /// </summary>
        public virtual void Dispose() { }

        #endregion
    }
}
