/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2015, Jerson Leonardo Huerfano Romero

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

namespace System.MicroFramework.Actuators
{

    /// <summary>
    /// Abstraction of DC motor driver.
    /// </summary>
    public class DCMotor : IDevice
    {
        /// <summary>
        /// Creates a new instance of DCMotor
        /// </summary>
        /// <param name="controlChannel">PWM channel of the control port.</param>
        /// <param name="directionPin">Pin of the direction port.</param>
        /// <param name="frequency">Frequency of the PWM signal, in Hz.</param>
        /// <param name="invertDirection">if true, the positive direction will be treated as MotorDirection.Backward.</param>
        /// <param name="invertPWMSignal">if true, the PWM signal will be set as invert.</param>
        public DCMotor(Cpu.PWMChannel controlChannel, Cpu.Pin directionPin, int frequency, bool invertDirection, bool invertPWMSignal)
        {
            this.ControlChannel = controlChannel;
            this.DirectionPin = directionPin;
            this.Frequency = frequency;
            this.InvertDirection = invertDirection;
            this.InvertPWMSignal = invertPWMSignal;
        }

        /// <summary>
        /// Creates a new instance of DCMotor
        /// </summary>
        /// <param name="controlChannel">PWM channel of the control port.</param>
        /// <param name="directionPin">Pin of the direction port.</param>
        /// <param name="frequency">Frequency of the PWM signal, in Hz.</param>
        /// <param name="invertDirection">if true, the positive direction will be treated as MotorDirection.Backward.</param>
        public DCMotor(Cpu.PWMChannel controlChannel, Cpu.Pin directionPin, int frequency, bool invertDirection)
            : this(controlChannel, directionPin, frequency, invertDirection, false) { }

        /// <summary>
        /// Creates a new instance of DCMotor
        /// </summary>
        /// <param name="controlChannel">PWM channel of the control port.</param>
        /// <param name="directionPin">Pin of the direction port.</param>
        /// <param name="frequency">Frequency of the PWM signal, in Hz.</param>
        public DCMotor(Cpu.PWMChannel controlChannel, Cpu.Pin directionPin, int frequency)
            : this(controlChannel, directionPin, frequency, false, false) { }

        /// <summary>
        /// Gets the current PWM frequency of the signal, in Hz.
        /// </summary>
        public int Frequency { get; private set; }
        /// <summary>
        /// Gets the current control channel of the control port.
        /// </summary>
        public Cpu.PWMChannel ControlChannel { get; private set; }
        private PWM ControlPort { get; set; }
        /// <summary>
        /// Gets the current direction pin of the direction port.
        /// </summary>
        public Cpu.Pin DirectionPin { get; private set; }
        private OutputPort DirectionPort { get; set; }

        /// <summary>
        /// Gets a value that indicates if the direction of the motor is inverted.
        /// </summary>
        public bool InvertDirection { get; private set; }
        /// <summary>
        /// Gets a value that indicates if the PWM signal is inverted.
        /// </summary>
        public bool InvertPWMSignal { get; private set; }

        /// <summary>
        /// Sets the output of the DCMotor.
        /// </summary>
        /// <param name="value">Value between 0-1; if is negative, sets the opposite direction.</param>
        public void SetSpeed(float value)
        {
            double absv = System.Math.Abs(value);

            DirectionPort.Write((InvertDirection) ? value > 0f : value < 0f);
            ControlPort.DutyCycle = (absv > 1) ? 1 : absv;
        }
        /// <summary>
        /// Method that is exposed for attaching to a ControllerBase instance. Not intended to be used by user.
        /// </summary>
        /// <param name="controller">ControllerBase instance.</param>
        /// <param name="e">Control event arguments.</param>
        public void OnControllerStateChanged(IDevice controller, Control.ControllerStateChangedEventArgs e)
        {
            SetSpeed(e.ControlSignal);
        }

        #region Miembros de IDevice

        /// <summary>
        /// Gets or sets the ID of this device.
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Initilize the DCMotor.
        /// </summary>
        void IDevice.Initialize()
        {
            ControlPort = new PWM(ControlChannel, Frequency, 0, InvertPWMSignal);
            DirectionPort = new OutputPort(DirectionPin, !InvertDirection);
        }

        #endregion
        #region Miembros de IDisposable

        /// <summary>
        /// Dispose the DCMotor resources.
        /// </summary>
        public void Dispose()
        {
            ControlPort.Stop();
            ControlPort.Dispose();
            ControlPort = null;
            DirectionPort.Write(false);
            DirectionPort.Dispose();
            DirectionPort = null;

            Debug.GC(true);
        }

        #endregion
    }
}
