/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2015, Jerson Leonardo Huerfano Romero

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using System.IO.Ports;

namespace System.MicroFramework.Serial
{
    /// <summary>
    /// Delegate for LineReceived events.
    /// </summary>
    /// <param name="sender">Instance of the CommandPort</param>
    /// <param name="line">Buffer that contains the received line.</param>
    /// <param name="byteCount">Amount of bytes of the received line</param>
    public delegate void LineReceivedEventHandler(CommandPort sender, byte[] line, int byteCount);

    /// <summary>
    /// Implements a commmand receiver functionality using Serial Port.
    /// </summary>
    public class CommandPort
    {
        /// <summary>
        /// Occurs when a new line was received.
        /// </summary>
        public event LineReceivedEventHandler LineReceived;

        /// <summary>
        /// Creates a new instace of CommandPort
        /// </summary>
        /// <param name="bufferSize">Size of the buffer.</param>
        /// <param name="port">SerialPort instance.</param>
        public CommandPort(int bufferSize, SerialPort port)
        {
            InputBuffer = new byte[bufferSize];

            SerialPort = port;
            SerialPort.DataReceived += SerialPort_DataReceived;
        }
        /// <summary>
        /// Creates a new instance of CommandPort.
        /// </summary>
        /// <param name="bufferSize">Size of the buffer.</param>
        /// <param name="portName">Name of the port.</param>
        /// <param name="baudRate">Baud rate.</param>
        public CommandPort(int bufferSize, string portName, int baudRate)
            :this(bufferSize, new SerialPort(portName, baudRate)) { }

        void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int byteCount = SerialPort.Read(InputBuffer, Cursor, InputBuffer.Length - Cursor);

            for (int i = 0; i < byteCount; i++)
            {
                if (IsEOL(InputBuffer, Cursor))
                {
                    OnLineReceived(Cursor - 1);
                    LeftMove(InputBuffer, Cursor + 1, byteCount - i - 1);
                    Cursor = -1;
                }
                Cursor++;
            }
        }
        
        /// <summary>
        /// Gets or sets the cursor position.
        /// </summary>
        public int Cursor { get; set; }
        /// <summary>
        /// Gets the input buffer.
        /// </summary>
        public byte[] InputBuffer { get; private set; }
        /// <summary>
        /// Gets the serial port instance.
        /// </summary>
        public SerialPort SerialPort { get; private set; }
        
        private void OnLineReceived(int byteCount)
        {
            if (LineReceived != null)
                LineReceived(this, InputBuffer, byteCount);
        }
        /// <summary>
        /// Write a response to the serial port.
        /// </summary>
        /// <param name="data">Data output.</param>
        /// <param name="count">Data size, in bytes.</param>
        public void Response(byte[] data, int count)
        {
            SerialPort.Write(data, 0, count);
        }
        /// <summary>
        /// Gets or sets the EOL bytes. Default is \r\n
        /// </summary>
        public static byte[] EOL = new byte[] { 0x04, 0x0D };
        /// <summary>
        /// Check if an EOL is present in the current position.
        /// </summary>
        /// <param name="data">Data buffer.</param>
        /// <param name="currentByte">Current position.</param>
        /// <returns></returns>
        public static bool IsEOL(byte[] data, int currentByte)
        {
            if (currentByte - 1 < 0 || currentByte >= data.Length) return false;
            else return data[currentByte - 1] == EOL[0] && data[currentByte] == EOL[1];
        }
        /// <summary>
        /// Move the initial position of the buffer.
        /// </summary>
        /// <param name="data">Data buffer.</param>
        /// <param name="offset">New initial position</param>
        /// <param name="count">Number of bytes that remains on right side of the new initial position</param>
        public static void LeftMove(byte[] data, int offset, int count)
        {
            for (int i = 0; i < count; i++)
                data[i] = data[offset + i];
        }
    }
}
