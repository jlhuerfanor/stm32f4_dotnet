/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2015, Jerson Leonardo Huerfano Romero

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Microsoft.SPOT;

namespace System.MicroFramework
{
    /// <summary>
    /// Extensions for MFApplication
    /// </summary>
    public static class MFApplicationBaseExtensions
    {
        /// <summary>
        /// Converts a TimeSpan instance to equivalent seconds.
        /// </summary>
        /// <param name="timeSpan">Instance of TimeSpan.</param>
        /// <returns></returns>
        public static float ToSeconds(this TimeSpan timeSpan)
        {
            return (timeSpan.Days * 86400f) + (timeSpan.Hours * 3600f) + (timeSpan.Minutes * 60f) + timeSpan.Seconds + (timeSpan.Milliseconds / 1000f); 
        }
    }
}
