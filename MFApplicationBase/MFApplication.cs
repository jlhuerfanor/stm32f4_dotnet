/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2015, Jerson Leonardo Huerfano Romero

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using Microsoft.SPOT;

namespace System.MicroFramework
{
    /// <summary>
    /// Defines the base class of a MicroFramework application.
    /// </summary>
    public abstract class MFApplication : IDisposable
    {
        /// <summary>
        /// Create a new instance of MFApplication.
        /// </summary>
        public MFApplication()
        {
            Devices = new Stack();
            IsActiveContext = false;
        }

        /// <summary>
        /// Gets the stack of IDevice instances attached to the application.
        /// </summary>
        public Stack Devices { get; private set; }
        /// <summary>
        /// Gets a value that indicates if the application is on active context (running).
        /// </summary>
        public bool IsActiveContext { get; private set; }
        /// <summary>
        /// Gets the Thread instance of this application.
        /// </summary>
        public Threading.Thread AppThread { get; private set; }
        /// <summary>
        /// Setups the application.
        /// </summary>
        protected abstract void ApplicationSetup();
        /// <summary>
        /// Startup the application.
        /// </summary>
        protected abstract void ApplicationStartup();

        /// <summary>
        /// Registers a new IDevie instance to the application.
        /// </summary>
        /// <param name="device">IDevice instance to be registered</param>
        protected void Register(IDevice device)
        {
            if (device != null) 
                Devices.Push(device);
        }
        /// <summary>
        /// Run the application.
        /// </summary>
        public void Run()
        {
            if (IsActiveContext) throw new ApplicationException("Application is already running.");
            
            ApplicationSetup();                    // Setup all devices in stack

            foreach (IDevice item in Devices)
                item.Initialize();      // Initialize all devices in stack.

            IsActiveContext = true;     // Begin active context.
            ApplicationStartup();                  // Start the application routines.
            IsActiveContext = false;    // Finalize active context.
            Dispose();                  // Dispose the application resources.
        }
        /// <summary>
        /// Run the application in a new thread.
        /// </summary>
        /// <param name="priority">Thread priority. Default value is ThreadPriority.Normal</param>
        public void RunAsNewThread(Threading.ThreadPriority priority = Threading.ThreadPriority.Normal)
        {
            if (AppThread != null && AppThread.IsAlive) return;

            AppThread = new Threading.Thread(new Threading.ThreadStart(Run))
            {
                Priority = priority
            };
            AppThread.Start();
        }

        #region Miembros de IDisposable

        /// <summary>
        /// Disposes all devices registered in the application.
        /// </summary>
        public virtual void Dispose()
        {
            foreach (IDevice item in Devices)
                item.Dispose();
        }

        #endregion

        //--------------------------------------------------------
        /// <summary>
        /// Gets or sets the current application of the process.
        /// </summary>
        public static MFApplication Current { get; set; }
        /// <summary>
        /// Gets the amount of free memory of the microcontroller.
        /// </summary>
        public static uint FreeMemory
        { 
            get { return Debug.GC(false); } 
        }
    }
}
