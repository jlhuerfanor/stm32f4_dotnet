using System;
using Microsoft.SPOT;
using System.MicroFramework.SignalProcessing;
using ARM.CMSIS.DSP;

namespace Microsoft.SPOT.Hardware.STM32
{
    /// <summary>
    /// Implements a FIR filter from CMSIS DSP Library: 
    /// y[n] = b[0] * x[n] + b[1] * x[n-1] + b[2] * x[n-2] + ...+ b[numTaps-1] * x[n-numTaps+1] 
    /// </summary>
    public class FIRFilter : IFilter
    {
        private float[] coeffs;
        private float[] states;
        private uint maxBlockSize;

        /// <summary>
        /// Creates a new instance of FIRFilter
        /// </summary>
        /// <param name="maxBlockSize">Maximum block size, must be greater or equals than 1.</param>
        /// <param name="coeffs">Coefficients of the filter, stored in reverse time order: 
        /// {b[numTaps-1], b[numTaps-2], b[N-2], ..., b[1], b[0]}.</param>
        public FIRFilter(uint maxBlockSize, params float[] coeffs)
        {
            this.maxBlockSize = maxBlockSize;
            this.coeffs = coeffs;
            this.states = new float[coeffs.Length + maxBlockSize - 1];
        }

        /// <summary>
        /// Gets the coefficient vector of the filter in reverse time order: 
        /// {b[numTaps-1], b[numTaps-2], b[N-2], ..., b[1], b[0]}.
        /// </summary>
        public float[] Coefficients
        {
            get { return coeffs; }
        }
        /// <summary>
        /// Gets the state vector of the filter in reverese time order:
        /// {x[n-numTaps+1], x[n-numTaps], x[n-numTaps-1], x[n-numTaps-2]....x[0], x[1], ..., x[blockSize-1]}.
        /// </summary>
        public float[] States
        {
            get { return states; }
        }
        /// <summary>
        /// Gets the maximum block size to be processed per filter call. Usefull for 
        /// multiple input sample filtering.
        /// </summary>
        public uint BlockSize
        {
            get { return maxBlockSize; }
        }

        #region Miembros de IFilter

        /// <summary>
        /// Filters the input data.
        /// </summary>
        /// <param name="input">Input data.</param>
        /// <returns></returns>
        public float Process(float input)
        {
            return ARM.CMSIS.DSP.FIRFilterARM.ProcessARM(Coefficients, States, input);
        }
        /// <summary>
        /// Filters a set of data and stores the result on a vector.
        /// </summary>
        /// <param name="input">Input vector.</param>
        /// <param name="output">Output vector.</param>
        public void Process(float[] input, float[] output)
        {
            ARM.CMSIS.DSP.FIRFilterARM.ProcessARM(coeffs, states, input, output);
        }

        #endregion

        #region Miembros de IDisposable

        /// <summary>
        /// Dispose the resources used by the filter.
        /// </summary>
        public void Dispose()
        {
            this.coeffs = null;
            this.states = null;
        }

        #endregion
    }
}
