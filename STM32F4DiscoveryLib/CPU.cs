////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
//
//  Copyright (c) Microsoft Corporation. All rights reserved.
//  Implementation for STM32: Copyright (c) Oberon microsystems, Inc.
//  
//  CPU classes in Microsoft.SPOT.Hardware.STM32 namespace
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Threading;
using System.Runtime.CompilerServices;
using Microsoft.SPOT.Hardware;
using System.IO.Ports;

namespace Microsoft.SPOT.Hardware.STM32
{

    /// <summary>
    /// Defines identifiers for hardware I/O pins.
    /// </summary>
    public static class Pins
    {
        /// <summary>
        /// GPIO port A bit 0
        /// </summary>
        public const Cpu.Pin GPIO_PA0 = (Cpu.Pin)0;
        /// <summary>
        /// GPIO port A bit 1
        /// </summary>
        public const Cpu.Pin GPIO_PA1 = (Cpu.Pin)1;
        /// <summary>
        /// GPIO port A bit 2
        /// </summary>
        public const Cpu.Pin GPIO_PA2 = (Cpu.Pin)2;
        /// <summary>
        /// GPIO port A bit 3
        /// </summary>
        public const Cpu.Pin GPIO_PA3 = (Cpu.Pin)3;
        /// <summary>
        /// GPIO port A bit 4
        /// </summary>
        public const Cpu.Pin GPIO_PA4 = (Cpu.Pin)4;
        /// <summary>
        /// GPIO port A bit 5
        /// </summary>
        public const Cpu.Pin GPIO_PA5  = (Cpu.Pin)5;
        /// <summary>
        /// GPIO port A bit 6
        /// </summary>
        public const Cpu.Pin GPIO_PA6 = (Cpu.Pin)6;
        /// <summary>
        /// GPIO port A bit 7
        /// </summary>
        public const Cpu.Pin GPIO_PA7 = (Cpu.Pin)7;
        /// <summary>
        /// GPIO port A bit 8
        /// </summary>
        public const Cpu.Pin GPIO_PA8 = (Cpu.Pin)8;
        /// <summary>
        /// GPIO port A bit 9
        /// </summary>
        public const Cpu.Pin GPIO_PA9  = (Cpu.Pin)9;
        /// <summary>
        /// GPIO port A bit 10
        /// </summary>
        public const Cpu.Pin GPIO_PA10 = (Cpu.Pin)10;
        /// <summary>
        /// GPIO port A bit 11
        /// </summary>
        public const Cpu.Pin GPIO_PA11 = (Cpu.Pin)11;
        /// <summary>
        /// GPIO port A bit 12
        /// </summary>
        public const Cpu.Pin GPIO_PA12 = (Cpu.Pin)12;
        /// <summary>
        /// GPIO port A bit 13
        /// </summary>
        public const Cpu.Pin GPIO_PA13 = (Cpu.Pin)13;
        /// <summary>
        /// GPIO port A bit 14
        /// </summary>
        public const Cpu.Pin GPIO_PA14 = (Cpu.Pin)14;
        /// <summary>
        /// GPIO port A bit 15
        /// </summary>
        public const Cpu.Pin GPIO_PA15  = (Cpu.Pin)15;
        /// <summary>
        /// GPIO port B bit 0
        /// </summary>
        public const Cpu.Pin GPIO_PB0 = (Cpu.Pin)16;
        /// <summary>
        /// GPIO port B bit 1
        /// </summary>
        public const Cpu.Pin GPIO_PB1 = (Cpu.Pin)17;
        /// <summary>
        /// GPIO port B bit 2
        /// </summary>
        public const Cpu.Pin GPIO_PB2 = (Cpu.Pin)18;
        /// <summary>
        /// GPIO port B bit 3
        /// </summary>
        public const Cpu.Pin GPIO_PB3 = (Cpu.Pin)19;
        /// <summary>
        /// GPIO port B bit 4
        /// </summary>
        public const Cpu.Pin GPIO_PB4 = (Cpu.Pin)20;
        /// <summary>
        /// GPIO port B bit 5
        /// </summary>
        public const Cpu.Pin GPIO_PB5 = (Cpu.Pin)21;
        /// <summary>
        /// GPIO port B bit 6
        /// </summary>
        public const Cpu.Pin GPIO_PB6 = (Cpu.Pin)22;
        /// <summary>
        /// GPIO port B bit 7
        /// </summary>
        public const Cpu.Pin GPIO_PB7 = (Cpu.Pin)23;
        /// <summary>
        /// GPIO port B bit 8
        /// </summary>
        public const Cpu.Pin GPIO_PB8 = (Cpu.Pin)24;
        /// <summary>
        /// GPIO port B bit 9
        /// </summary>
        public const Cpu.Pin GPIO_PB9 = (Cpu.Pin)25;
        /// <summary>
        /// GPIO port B bit 10
        /// </summary>
        public const Cpu.Pin GPIO_PB10 = (Cpu.Pin)26;
        /// <summary>
        /// GPIO port B bit 11
        /// </summary>
        public const Cpu.Pin GPIO_PB11 = (Cpu.Pin)27;
        /// <summary>
        /// GPIO port B bit 12
        /// </summary>
        public const Cpu.Pin GPIO_PB12 = (Cpu.Pin)28;
        /// <summary>
        /// GPIO port B bit 13
        /// </summary>
        public const Cpu.Pin GPIO_PB13 = (Cpu.Pin)29;
        /// <summary>
        /// GPIO port B bit 14
        /// </summary>
        public const Cpu.Pin GPIO_PB14 = (Cpu.Pin)30;
        /// <summary>
        /// GPIO port B bit 15
        /// </summary>
        public const Cpu.Pin GPIO_PB15 = (Cpu.Pin)31;
        /// <summary>
        /// GPIO port C bit 0
        /// </summary>
        public const Cpu.Pin GPIO_PC0 = (Cpu.Pin)32;
        /// <summary>
        /// GPIO port C bit 1
        /// </summary>
        public const Cpu.Pin GPIO_PC1 = (Cpu.Pin)33;
        /// <summary>
        /// GPIO port C bit 2
        /// </summary>
        public const Cpu.Pin GPIO_PC2 = (Cpu.Pin)34;
        /// <summary>
        /// GPIO port C bit 3
        /// </summary>
        public const Cpu.Pin GPIO_PC3 = (Cpu.Pin)35;
        /// <summary>
        /// GPIO port C bit 4
        /// </summary>
        public const Cpu.Pin GPIO_PC4 = (Cpu.Pin)36;
        /// <summary>
        /// GPIO port C bit 5
        /// </summary>
        public const Cpu.Pin GPIO_PC5 = (Cpu.Pin)37;
        /// <summary>
        /// GPIO port C bit 6
        /// </summary>
        public const Cpu.Pin GPIO_PC6 = (Cpu.Pin)38;
        /// <summary>
        /// GPIO port C bit 7
        /// </summary>
        public const Cpu.Pin GPIO_PC7 = (Cpu.Pin)39;
        /// <summary>
        /// GPIO port C bit 8
        /// </summary>
        public const Cpu.Pin GPIO_PC8 = (Cpu.Pin)40;
        /// <summary>
        /// GPIO port C bit 9
        /// </summary>
        public const Cpu.Pin GPIO_PC9 = (Cpu.Pin)41;
        /// <summary>
        /// GPIO port C bit 10
        /// </summary>
        public const Cpu.Pin GPIO_PC10 = (Cpu.Pin)42;
        /// <summary>
        /// GPIO port C bit 11
        /// </summary>
        public const Cpu.Pin GPIO_PC11 = (Cpu.Pin)43;
        /// <summary>
        /// GPIO port C bit 12
        /// </summary>
        public const Cpu.Pin GPIO_PC12 = (Cpu.Pin)44;
        /// <summary>
        /// GPIO port C bit 13
        /// </summary>
        public const Cpu.Pin GPIO_PC13 = (Cpu.Pin)45;
        /// <summary>
        /// GPIO port C bit 14
        /// </summary>
        public const Cpu.Pin GPIO_PC14 = (Cpu.Pin)46;
        /// <summary>
        /// GPIO port C bit 15
        /// </summary>
        public const Cpu.Pin GPIO_PC15 = (Cpu.Pin)47;
        /// <summary>
        /// GPIO port D bit 0
        /// </summary>
        public const Cpu.Pin GPIO_PD0 = (Cpu.Pin)48;
        /// <summary>
        /// GPIO port D bit 1
        /// </summary>
        public const Cpu.Pin GPIO_PD1 = (Cpu.Pin)49;
        /// <summary>
        /// GPIO port D bit 2
        /// </summary>
        public const Cpu.Pin GPIO_PD2 = (Cpu.Pin)50;
        /// <summary>
        /// GPIO port D bit 3
        /// </summary>
        public const Cpu.Pin GPIO_PD3 = (Cpu.Pin)51;
        /// <summary>
        /// GPIO port D bit 4
        /// </summary>
        public const Cpu.Pin GPIO_PD4 = (Cpu.Pin)52;
        /// <summary>
        /// GPIO port D bit 5
        /// </summary>
        public const Cpu.Pin GPIO_PD5 = (Cpu.Pin)53;
        /// <summary>
        /// GPIO port D bit 6
        /// </summary>
        public const Cpu.Pin GPIO_PD6 = (Cpu.Pin)54;
        /// <summary>
        /// GPIO port D bit 7
        /// </summary>
        public const Cpu.Pin GPIO_PD7 = (Cpu.Pin)55;
        /// <summary>
        /// GPIO port D bit 8
        /// </summary>
        public const Cpu.Pin GPIO_PD8 = (Cpu.Pin)56;
        /// <summary>
        /// GPIO port D bit 9
        /// </summary>
        public const Cpu.Pin GPIO_PD9 = (Cpu.Pin)57;
        /// <summary>
        /// GPIO port D bit 10
        /// </summary>
        public const Cpu.Pin GPIO_PD10 = (Cpu.Pin)58;
        /// <summary>
        /// GPIO port D bit 11
        /// </summary>
        public const Cpu.Pin GPIO_PD11 = (Cpu.Pin)59;
        /// <summary>
        /// GPIO port D bit 12
        /// </summary>
        public const Cpu.Pin GPIO_PD12 = (Cpu.Pin)60;
        /// <summary>
        /// GPIO port D bit 13
        /// </summary>
        public const Cpu.Pin GPIO_PD13 = (Cpu.Pin)61;
        /// <summary>
        /// GPIO port D bit 14
        /// </summary>
        public const Cpu.Pin GPIO_PD14 = (Cpu.Pin)62;
        /// <summary>
        /// GPIO port D bit 15
        /// </summary>
        public const Cpu.Pin GPIO_PD15 = (Cpu.Pin)63;
        /// <summary>
        /// GPIO port E bit 0
        /// </summary>
        public const Cpu.Pin GPIO_PE0 = (Cpu.Pin)64;
        /// <summary>
        /// GPIO port E bit 1
        /// </summary>
        public const Cpu.Pin GPIO_PE1 = (Cpu.Pin)65;
        /// <summary>
        /// GPIO port E bit 2
        /// </summary>
        public const Cpu.Pin GPIO_PE2 = (Cpu.Pin)66;
        /// <summary>
        /// GPIO port E bit 3
        /// </summary>
        public const Cpu.Pin GPIO_PE3 = (Cpu.Pin)67;
        /// <summary>
        /// GPIO port E bit 4
        /// </summary>
        public const Cpu.Pin GPIO_PE4 = (Cpu.Pin)68;
        /// <summary>
        /// GPIO port E bit 5
        /// </summary>
        public const Cpu.Pin GPIO_PE5 = (Cpu.Pin)69;
        /// <summary>
        /// GPIO port E bit 6
        /// </summary>
        public const Cpu.Pin GPIO_PE6 = (Cpu.Pin)70;
        /// <summary>
        /// GPIO port E bit 7
        /// </summary>
        public const Cpu.Pin GPIO_PE7 = (Cpu.Pin)71;
        /// <summary>
        /// GPIO port E bit 8
        /// </summary>
        public const Cpu.Pin GPIO_PE8 = (Cpu.Pin)72;
        /// <summary>
        /// GPIO port E bit 9
        /// </summary>
        public const Cpu.Pin GPIO_PE9 = (Cpu.Pin)73;
        /// <summary>
        /// GPIO port E bit 10
        /// </summary>
        public const Cpu.Pin GPIO_PE10 = (Cpu.Pin)74;
        /// <summary>
        /// GPIO port E bit 11
        /// </summary>
        public const Cpu.Pin GPIO_PE11 = (Cpu.Pin)75;
        /// <summary>
        /// GPIO port E bit 12
        /// </summary>
        public const Cpu.Pin GPIO_PE12 = (Cpu.Pin)76;
        /// <summary>
        /// GPIO port E bit 13
        /// </summary>
        public const Cpu.Pin GPIO_PE13 = (Cpu.Pin)77;
        /// <summary>
        /// GPIO port E bit 14
        /// </summary>
        public const Cpu.Pin GPIO_PE14 = (Cpu.Pin)78;
        /// <summary>
        /// GPIO port E bit 15
        /// </summary>
        public const Cpu.Pin GPIO_PE15 = (Cpu.Pin)79;
        /// <summary>
        /// GPIO port F bit 0
        /// </summary>
        public const Cpu.Pin GPIO_PF0 = (Cpu.Pin)80;
        /// <summary>
        /// GPIO port F bit 1
        /// </summary>
        public const Cpu.Pin GPIO_PF1 = (Cpu.Pin)81;
        /// <summary>
        /// GPIO port F bit 2
        /// </summary>
        public const Cpu.Pin GPIO_PF2 = (Cpu.Pin)82;
        /// <summary>
        /// GPIO port F bit 3
        /// </summary>
        public const Cpu.Pin GPIO_PF3 = (Cpu.Pin)83;
        /// <summary>
        /// GPIO port F bit 4
        /// </summary>
        public const Cpu.Pin GPIO_PF4 = (Cpu.Pin)84;
        /// <summary>
        /// GPIO port F bit 5
        /// </summary>
        public const Cpu.Pin GPIO_PF5 = (Cpu.Pin)85;
        /// <summary>
        /// GPIO port F bit 6
        /// </summary>
        public const Cpu.Pin GPIO_PF6 = (Cpu.Pin)86;
        /// <summary>
        /// GPIO port F bit 7
        /// </summary>
        public const Cpu.Pin GPIO_PF7 = (Cpu.Pin)87;
        /// <summary>
        /// GPIO port F bit 8
        /// </summary>
        public const Cpu.Pin GPIO_PF8 = (Cpu.Pin)88;
        /// <summary>
        /// GPIO port F bit 9
        /// </summary>
        public const Cpu.Pin GPIO_PF9 = (Cpu.Pin)89;
        /// <summary>
        /// GPIO port F bit 10
        /// </summary>
        public const Cpu.Pin GPIO_PF10 = (Cpu.Pin)90;
        /// <summary>
        /// GPIO port F bit 11
        /// </summary>
        public const Cpu.Pin GPIO_PF11 = (Cpu.Pin)91;
        /// <summary>
        /// GPIO port F bit 12
        /// </summary>
        public const Cpu.Pin GPIO_PF12 = (Cpu.Pin)92;
        /// <summary>
        /// GPIO port F bit 13
        /// </summary>
        public const Cpu.Pin GPIO_PF13 = (Cpu.Pin)93;
        /// <summary>
        /// GPIO port F bit 14
        /// </summary>
        public const Cpu.Pin GPIO_PF14 = (Cpu.Pin)94;
        /// <summary>
        /// GPIO port F bit 15
        /// </summary>
        public const Cpu.Pin GPIO_PF15 = (Cpu.Pin)95;
        /// <summary>
        /// GPIO port G bit 0
        /// </summary>
        public const Cpu.Pin GPIO_PG0 = (Cpu.Pin)96;
        /// <summary>
        /// GPIO port G bit 1
        /// </summary>
        public const Cpu.Pin GPIO_PG1 = (Cpu.Pin)97;
        /// <summary>
        /// GPIO port G bit 2
        /// </summary>
        public const Cpu.Pin GPIO_PG2 = (Cpu.Pin)98;
        /// <summary>
        /// GPIO port G bit 3
        /// </summary>
        public const Cpu.Pin GPIO_PG3 = (Cpu.Pin)99;
        /// <summary>
        /// GPIO port G bit 4
        /// </summary>
        public const Cpu.Pin GPIO_PG4 = (Cpu.Pin)100;
        /// <summary>
        /// GPIO port G bit 5
        /// </summary>
        public const Cpu.Pin GPIO_PG5 = (Cpu.Pin)101;
        /// <summary>
        /// GPIO port G bit 6
        /// </summary>
        public const Cpu.Pin GPIO_PG6 = (Cpu.Pin)102;
        /// <summary>
        /// GPIO port G bit 7
        /// </summary>
        public const Cpu.Pin GPIO_PG7 = (Cpu.Pin)103;
        /// <summary>
        /// GPIO port G bit 8
        /// </summary>
        public const Cpu.Pin GPIO_PG8 = (Cpu.Pin)104;
        /// <summary>
        /// GPIO port G bit 9
        /// </summary>
        public const Cpu.Pin GPIO_PG9 = (Cpu.Pin)105;
        /// <summary>
        /// GPIO port G bit 10
        /// </summary>
        public const Cpu.Pin GPIO_PG10 = (Cpu.Pin)106;
        /// <summary>
        /// GPIO port G bit 11
        /// </summary>
        public const Cpu.Pin GPIO_PG11 = (Cpu.Pin)107;
        /// <summary>
        /// GPIO port G bit 12
        /// </summary>
        public const Cpu.Pin GPIO_PG12 = (Cpu.Pin)108;
        /// <summary>
        /// GPIO port G bit 13
        /// </summary>
        public const Cpu.Pin GPIO_PG13 = (Cpu.Pin)109;
        /// <summary>
        /// GPIO port G bit 14
        /// </summary>
        public const Cpu.Pin GPIO_PG14 = (Cpu.Pin)110;
        /// <summary>
        /// GPIO port G bit 15
        /// </summary>
        public const Cpu.Pin GPIO_PG15 = (Cpu.Pin)111;
        /// <summary>
        /// GPIO None.
        /// </summary>
        public const Cpu.Pin GPIO_NONE = Cpu.Pin.GPIO_NONE;
    }

    /// <summary>
    /// Defines the avaiable serial ports.
    /// </summary>
    public static class SerialPorts
    {
        /// <summary>
        /// Serial port COM1. Try not to use this port while USB Debugging
        /// </summary>
        public const string COM1 = "COM1";
        /// <summary>
        /// Serial port COM2.
        /// </summary>
        public const string COM2 = "COM2";
        /// <summary>
        /// Serial port COM3.
        /// </summary>
        public const string COM3 = "COM3";
    }
    /// <summary>
    /// Defines the avaiable analog channels
    /// </summary>
    public static class AnalogChannels
    {
        // channel:   0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15
        // AD pins: PA0,PA1,PA2,PA3,PA4,PA5,PA6,PA7,PB0,PB1,PC0,PC1,PC2,PC3,PC4,PC5
        /// <summary>
        /// Analog channel 0 (Pin PA0)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.AnalogChannel PA0 = (Cpu.AnalogChannel)(0);
        /// <summary>
        /// Analog channel 1 (Pin PA1)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.AnalogChannel PA1 = (Cpu.AnalogChannel)(1);
        /// <summary>
        /// Analog channel 2 (Pin PA2)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.AnalogChannel PA2 = (Cpu.AnalogChannel)(2);
        /// <summary>
        /// Analog channel 3 (Pin PA3)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.AnalogChannel PA3 = (Cpu.AnalogChannel)(3);
        /// <summary>
        /// Analog channel 4 (Pin PA4)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.AnalogChannel PA4 = (Cpu.AnalogChannel)(4);
        /// <summary>
        /// Analog channel 5 (Pin PA5)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.AnalogChannel PA5 = (Cpu.AnalogChannel)(5);
        /// <summary>
        /// Analog channel 6 (Pin PA6)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.AnalogChannel PA6 = (Cpu.AnalogChannel)(6);
        /// <summary>
        /// Analog channel 7 (Pin PA7)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.AnalogChannel PA7 = (Cpu.AnalogChannel)(7);
        /// <summary>
        /// Analog channel 8 (Pin PB0)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.AnalogChannel PB0 = (Cpu.AnalogChannel)(8);
        /// <summary>
        /// Analog channel 9 (Pin PB1)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.AnalogChannel PB1 = (Cpu.AnalogChannel)(9);
        /// <summary>
        /// Analog channel 10 (Pin PC0)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.AnalogChannel PC0 = (Cpu.AnalogChannel)(10);
        /// <summary>
        /// Analog channel 11 (Pin PC1)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.AnalogChannel PC1 = (Cpu.AnalogChannel)(11);
        /// <summary>
        /// Analog channel 12 (Pin PC2)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.AnalogChannel PC2 = (Cpu.AnalogChannel)(12);
        /// <summary>
        /// Analog channel 13 (Pin PC3)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.AnalogChannel PC3 = (Cpu.AnalogChannel)(13);
        /// <summary>
        /// Analog channel 14 (Pin PC4)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.AnalogChannel PC4 = (Cpu.AnalogChannel)(14);
        /// <summary>
        /// Analog channel 15 (Pin PC5)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.AnalogChannel PC5 = (Cpu.AnalogChannel)(15);
    }
    /// <summary>
    /// Defines the PWM channels
    /// </summary>
    public static class PWMChannels
    {
        // D12-D15,E9,E11,E13,E14
        /// <summary>
        /// PWM Channel 0 (Pin PD12)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.PWMChannel PD12 = Cpu.PWMChannel.PWM_0;
        /// <summary>
        /// PWM Channel 1 (Pin PD13)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.PWMChannel PD13 = Cpu.PWMChannel.PWM_1;
        /// <summary>
        /// PWM Channel 2 (Pin PD14)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.PWMChannel PD14 = Cpu.PWMChannel.PWM_2;
        /// <summary>
        /// PWM Channel 3 (Pin PD15)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.PWMChannel PD15 = Cpu.PWMChannel.PWM_3;
        /// <summary>
        /// PWM Channel 4 (Pin PE9)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.PWMChannel PE9 = Cpu.PWMChannel.PWM_4;
        /// <summary>
        /// PWM Channel 5 (Pin PE11)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.PWMChannel PE11 = Cpu.PWMChannel.PWM_5;
        /// <summary>
        /// PWM Channel 6 (Pin PE13)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.PWMChannel PE13 = Cpu.PWMChannel.PWM_6;
        /// <summary>
        /// PWM Channel 7 (Pin PE14)
        /// </summary>
        public const Microsoft.SPOT.Hardware.Cpu.PWMChannel PE14 = Cpu.PWMChannel.PWM_7;
    }
}
