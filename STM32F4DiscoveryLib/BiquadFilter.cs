/////////////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2015, Jerson Leonardo Huerfano Romero

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
/////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Runtime.CompilerServices;
using Microsoft.SPOT;

namespace Microsoft.SPOT.Hardware.STM32
{
    /// <summary>
    /// Implements a Biquad Cascade IIR Filter from CMSIS DSP Library.
    /// </summary>
    public class BiquadFilter : System.MicroFramework.SignalProcessing.IFilter
    {
        private uint nStages;
        private float[] coefs;
        private float[] states;

        /// <summary>
        /// Creates a new Instance of BiquadFilter.
        /// </summary>
        /// <param name="numOfStages">Number of stages of the Biquad Filter</param>
        public BiquadFilter(uint numOfStages)
        {
            nStages = numOfStages;
            coefs = new float[5 * numOfStages];
            states = new float[4 * numOfStages];
        }

        /// <summary>
        /// Sets the value of the coefficient at specific stage.
        /// </summary>
        /// <param name="stage">Stage index.</param>
        /// <param name="index">Index of the coefficient. the coefficients are arranged as
        /// {b0, b1, b2, a1, a2}.</param>
        /// <param name="value">New value of the coefficient.</param>
        public void SetCoefficient(uint stage, int index, float value)
        {
            #pragma warning disable
            if (stage < nStages && 0 <= index && index < 5)
                coefs[5 * stage + index] = value;
            else throw new IndexOutOfRangeException();
            #pragma warning restore
        }
        /// <summary>
        /// Gets the value of the coefficient at specific stage.
        /// </summary>
        /// <param name="stage">Stage index.</param>
        /// <param name="index">Index of the coefficient. the coefficients are arranged as
        /// {b0, b1, b2, a1, a2}.</param>
        public float GetCoefficient(uint stage, int index)
        {
            #pragma warning disable
            if (stage < nStages && 0 <= index && index < 5)
                return coefs[5 * stage + index];
            else throw new IndexOutOfRangeException();
            #pragma warning restore
        }
        /// <summary>
        /// Sets all coeficients of the filter.
        /// </summary>
        /// <param name="values">Values of the filter. The coefficients are arranged as
        /// {b00, b01, b02, a01, a02, b10, b11, b12, a11, a12, ... , bn0, bn1, bn2, an1, an2}.</param>
        public void SetAllCoefficients(params float[] values)
        {
            if (values.Length == nStages * 5)
                for (int i = 0; i < nStages * 5; i++)
                    SetCoefficient((uint)(i / 5), i % 5, values[i]);
            else throw new IndexOutOfRangeException();
        }

        #region Miembros de IFilter

        /// <summary>
        /// Process the input and get the filtered output.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public float Process(float input)
        {
            return ARM.CMSIS.DSP.BiquadFilterARM.ProcessARM(nStages, states, coefs, input);
        }


        #endregion
        #region Miembros de IDisposable
        /// <summary>
        /// Dispose all resources used by the filter.
        /// </summary>
        public void Dispose() { }

        #endregion

        #region Miembros de IFilter

        /// <summary>
        /// Process the input and store the filtered output ona vector.
        /// </summary>
        /// <param name="input">Input vector.</param>
        /// <param name="output">Output vetor.</param>
        public void Process(float[] input, float[] output)
        {
            ARM.CMSIS.DSP.BiquadFilterARM.ProcessARM(nStages, states, coefs, input, output);
        }

        #endregion
    }
}
